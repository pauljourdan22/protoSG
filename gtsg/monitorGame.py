from gtnash.game.normalformgame import NFG
import math
from gtsg.stochastic_game import SG,StateSG
from copy import deepcopy
import itertools
import numpy as np
from fractions import Fraction


class MonitorSG:
    """

    :param int n_players: Number of players
    :param int m_sites: Number of sites/actions
    :param list sv: List of value of site for each state
    :param list prob:
    :param np.array dista:

    """
    def __init__(self, n_players, m_sites, sv, prob, dista):
        self.n_players = n_players
        self.m_sites = m_sites
        self.site_value = {}
        self.site_valuelist = []
        for m in range(m_sites):
            self.site_value[m] = [sv[0][m], sv[1][m]]
            self.site_valuelist += [[sv[0][m], sv[1][m]]]
        self.dista = dista
        # All Possible site status combination
        # [(0,0,0),(0,0,1),(0,1,0),...,(1,1,1)]
        combi_action_status = itertools.product(
            *[range(2) for tsv in self.site_valuelist])
        # All Possible sites reward combination
        # [(sv,sv,sv),(sv,sv,av),(sv,av,sv),...,(av,av,av)]
        combi_actions_value = itertools.product(*self.site_valuelist)
        # Associate to each state id the sites reward and sites status
        self.stateid_to_v_ind = {}
        # Associate to each sites status a state id
        self.stateind_to_sid = {}
        all_jact=NFG(
            [[m for m in range(m_sites)]for n in range(n_players)],[]
        ).joint_actions.tolist()
        for state_i, (sv, sv_i) in enumerate(
                zip(combi_actions_value, combi_action_status)):
            self.stateid_to_v_ind[state_i] = (sv, sv_i)
            self.stateind_to_sid[sv_i] = state_i
        self.allgameslis = []
        self.proba_all = []
        self.computestategames(all_jact, prob)
        self.sg = SG(self.allgameslis, self.proba_all)

    def computestategames(self, all_jact, prob):
        """
        Method used to compute the utilities and transition of each game
        as a NFG and list

        :param list all_jact: List of the ordered joint action
        :param list prob: list of probability distribution over the site (PV)
        """
        combi_actions_value = itertools.product(*self.site_valuelist)
        combi_action_status = itertools.product(*[
            range(2) for tsv in self.site_valuelist])
        for state_i, (sv, sv_i) in enumerate(
                zip(combi_actions_value, combi_action_status)):
            # util = self.utilities_of_state(sv, all_jact)
            util = self.utilities_of_statealt(sv,sv_i, all_jact,prob)
            tmpnfg = NFG([[m for m in range(self.m_sites)]
                          for n in range(self.n_players)],
                         util)
            self.allgameslis += [tmpnfg]
            li_poss_sv_i = []
            nextstateid = []
            tmp_prod = []
            for ja_i, ja in enumerate(all_jact):
                # Get the action played (at least once) and sort them
                playedact = list(set(ja))
                playedact.sort()
                # Get all combination of reward index change for the
                # played actions (sorted)
                playedsite_change = itertools.product(
                    *[range(2) for tsv in playedact])
                # For the current state site status and the possible
                # status change, list all the status combination it is possible
                # to transition to
                reach_sv_i = [
                    [tmp_change[playedact.index(m)]
                     if m in playedact else sv_i[m]
                     for m in range(self.m_sites)
                     ]
                    for tmp_change in playedsite_change
                ]
                # Get the state id of the possible next state
                poss_nextid = [self.stateind_to_sid[tuple(svi)] for svi in
                               reach_sv_i]
                nextstateid += [poss_nextid]
                #[self.stateind_to_sid[tuple(svi)] for svi in poss_sv_i]
                li_poss_sv_i += [reach_sv_i]
                tmp_prod += [
                    [math.prod(
                        [prob[m][sv_i[m]][self.stateid_to_v_ind[s][1][m]]
                         if s in poss_nextid else 0
                         for m in playedact])
                     for s in range(len(self.stateid_to_v_ind.keys()))
                     ]
                ]
            self.proba_all += [tmp_prod]

    def utilities_of_state(self, sv, all_jact):
        """
        For a given set of site value, compute and return the utities

        :param tuple sv: Tuple of integer with the value of each sites
        :param list all_jact: List of all the joint action
        :return: List of utilities for each players
        :rtype: list
        """
        return [
            [sv[ja[n]] - self.dista[n, ja[n]]
             if ja[n] not in ja[:n] + ja[n + 1:] else
             sv[ja[n]] * 0 - self.dista[n, ja[n]]
             for ja in all_jact]
            for n in range(self.n_players)
        ]

    def utilities_of_statealt(self, sv,sv_i, all_jact,prob):
        """
        For a given set of site value, compute and return the utilities.
        This computation used the transition probabilities and value of the
        next possible state.

        :param tuple sv: Tuple of integer with the value of each sites
        :param tuple sv_i: Tuple of integer with the status of each sites
        :param list all_jact: List of the ordered joint action
        :param list prob: list of probability distribution over the site (PV)
        :return:
        """
        return [[(sv[ja[n]] - self.dista[n, ja[n]])
                 +sum([svi_next*prob[ja[n]][sv_i[ja[n]]][svi_next] for svi_next in [0,1]])
                 if ja[n] not in ja[:n] + ja[n + 1:] else
                 sv[ja[n]] * 0 - self.dista[n, ja[n]]
                 for ja in all_jact]
                for n in range(self.n_players)]

    def problem_of_policy(self):
        """

        :return:
        """
        self.sg.backwardRecursion(5)
