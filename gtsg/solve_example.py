from monitorGame import MonitorSG
from pddl_from_sg import ProblemMSG
import numpy as np
from fractions import Fraction

from gtsg.stochastic_game import SG
from gtnash.game.normalformgame import NFG
from gtnash.game.polymatrixgame import PMG
from gtnash.solver.lemkehowson_polymatrix import LHpolymatrix



######## Example Monitoring stochastic game float ########
## Creation of a Monitoring stochastic game 2 players, 4 sites.
# tmp_zeros =np.zeros((2 ,4))
# mobis =MonitorSG(2 ,4,
#                 [[0, 0, 0, 0], [1, 1, 1, 1]],
#                 [
#                  [[1-0.56, 0.56], [1-0.36, 0.36 ]],
#                  [[1-0.64, 0.64],  [1-0.35, 0.35]],
#                  [[1-0.855, 0.855], [1-0.64, 0.64]],
#                  [[1-0.42, 0.42], [1-0.42, 0.42]]
#                  ],
#                 tmp_zeros
#                 )
#
# print([s.s_game.utilities for s in mobis.sg.states.values()])
#
# probpddl=ProblemMSG(mobis,5)
# print("Policy:")
# print(probpddl.policy)
# probpddl.problem_gen((0,0,0,0),0)





############# Example Monitoring stochastic game Fraction #############
## Creation of a Monitoring stochastic game 2 players, 3 sites.
# tmp_zeros=np.zeros((2 ,3))
# mobis =MonitorSG(2 ,3,
#                 [[5, 3, 4], [6, 7, 6]],
#                 [[[Fraction(4,10), Fraction(6,10)], [Fraction(7,10), Fraction(3,10)]],
#                  [[Fraction(5,10), Fraction(5,10)], [Fraction(8,10), Fraction(2,10)]],
#                  [[Fraction(4,10), Fraction(6,10)], [Fraction(7,10), Fraction(3,10)]]],
#                 tmp_zeros)
# policy=mobis.sg.backwardRecursion(5)
# print(policy)





############# Example Stochastic game float  #############
## Creation of a stochastic game with 3 state,2 player and 2 action per player in each state
# nfgbis = NFG([[0, 1], [0, 1]], [[2, 0, 3, 4], [0, 1, 5, 4]])
# nfgbis2 = NFG([[0, 1], [0, 1]], [[3, 5, 1, 4], [4, 1, 1, 2]])
# nfgbis3 = NFG([[0, 1], [0, 1]], [[3, 2, 6, 7], [6, 5, 0, 3]])
# probatrans = [[[1, 0, 0],[0,0.5,0.5],[0.25,0.25,0.5],[0,0,1]],
#             [[0, 1, 0],[0.5,0,0.5],[0.5,0.25,0.25],[1,0,0]],
#             [[0, 0, 1],[0.5,0.5,0],[0.25,0.5,0.25],[0,1,0]]]
# sgtest = SG([nfgbis, nfgbis2, nfgbis3], probatrans)
# sgtest.backwardRecursion(3)





################ Example Stochastic game Fraction #############
## Creation of a stochastic game with 3 state,2 player and 2 action per player in each state
# nfgbis = NFG([[0, 1], [0, 1]], [[2, 0, 3, 4], [0, 1, 5, 4]])
# nfgbis2 = NFG([[0, 1], [0, 1]], [[3, 5, 1, 4], [4, 1, 1, 2]])
# nfgbis3 = NFG([[0, 1], [0, 1]], [[3, 2, 6, 7], [6, 5, 0, 3]])
# probatrans = [[[1, 0, 0],[0,Fraction(1,2),Fraction(1,2)],[Fraction(1,4),Fraction(1,4),Fraction(1,2)],[0,0,1]],
#             [[0, 1, 0],[Fraction(1,2),0,Fraction(1,2)],[Fraction(1,2),Fraction(1,4),Fraction(1,4)],[1,0,0]],
#             [[0, 0, 1],[Fraction(1,2),Fraction(1,2),0],[Fraction(1,4),Fraction(1,2),Fraction(1,4)],[0,1,0]]]
# sgtest = SG([nfgbis, nfgbis2, nfgbis3], probatrans)
# # Error when the horizon is greater than 2
# sgtest.backwardRecursion(2)




######## Example of error of a game corresponding to the state of a monitoring stochastic game
# tmp_nfg=NFG([[0,1,2,3],[0,1,2,3]],[[0.0, 0.56, 0.56, 0.56, 0.64, 0.0, 0.64, 0.64, 0.855, 0.855, 0.0, 0.855, 0.42, 0.42, 0.42, 0.0], [0.0, 0.64, 0.855, 0.42, 0.56, 0.0, 0.855, 0.42, 0.56, 0.64, 0.0, 0.42, 0.56, 0.64, 0.855, 0.0]])
# tmp_pmg = PMG(tmp_nfg.players_actions,
#                       [tmp_nfg.utilities], [[0, 1]])
# lhpoly = LHpolymatrix(tmp_pmg)
# print( lhpoly.launch_solver([0, 0]))
# print(tmp_pmg.is_equilibrium(lhpoly.launch_solver([0, 0])))