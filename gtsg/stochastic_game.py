# from abc import ABC, abstractmethod

# class AbstractState(ABC):

from gtnash.game.normalformgame import NFG
from gtnash.game.polymatrixgame import PMG
from gtnash.solver.lemkehowson_polymatrix import LHpolymatrix
import math
from copy import deepcopy
import itertools
import numpy as np
from fractions import Fraction


#MDP to train and remember basic notion, somewhat generic
class MDP:
    """
    :param list states: List of the states
    :param list actions: List of sublist of actions for each states
    :param list rewards: List of dictionary mapping to each transition a reward
    :param list transition: List of dictionary mapping to each action and state\
    a transition probability
    """
    def __init__(self, states, actions, rewards, transition):
        self.states = {}
        # self.actions_of_state=
        for s_i,s in enumerate(states):
            self.states[s]=State(s,actions[s_i],rewards[s_i],transition[s_i])

    def get_actions_of_state(self,s_id):
        """
        Return the list of action given a state id

        :param int s_id: State id
        :return: List of action
        :rtype: list
        """
        return self.states[s_id].actions

    def get_reward_state_act(self,s_id,a_id):
        """
        Return the reward for a combination of state and action

        :param int s_id: State id
        :param int a_id: Action id
        :return: Reward of the action for the state
        :rtype: int
        """
        return self.states[s_id].reward_of_a(a_id)

    def get_trans_s_act_ns(self,s_id, a_id, ns_id):
        """
        Return the probability of reaching a certain state from a given state
        and using a given  action

        :param int s_id: Starting state id
        :param int a_id: Action id of the starting state
        :param int ns_id: Possible reachable state
        :return: Probability of transition
        :rtype: float
        """
        return self.states[s_id].trans_a_s(a_id, ns_id)

    #Value iteration algorithme
    def stateValueAlg(self,disco,epsi):
        """
        Implementation of the ValueIteration

        :param float disco: Discount value
        :param float epsi: Epsilon value
        :return: Computed policy
        :rtype: dictionary
        """
        stateval_dic={}
        for s in self.states.values():
            stateval_dic[s.state_id]=max([s.reward_of_a(a) for a in s.actions])
        policy={}
        t=0
        delta=math.inf
        while epsi<delta:
            delta=0
            tmp_stateval={}
            for s in self.states.values():
                val_max=-math.inf
                arg_max=None
                for a in s.actions:
                    tmpval=s.reward_of_a(a) + \
                           disco * sum([
                        stateval_dic[s.state_id]*s.trans_a_s(a, sp.state_id)
                        for sp in self.states.values()])
                    if tmpval > val_max:
                        arg_max = a
                        val_max = tmpval
                tmp_stateval[s.state_id] = val_max
                policy[s.state_id] = arg_max
                delta = max(delta,
                            abs(tmp_stateval[s.state_id]-
                                stateval_dic[s.state_id]))
            stateval_dic=tmp_stateval
        print(policy)
        return policy

    #Backward recursion algorithme
    def backwardRecursion(self,hori):
        """
        Implementation of the Backward recursion algorithm

        :param int hori: Maximum horizon used
        :return: List of the computed policy for each step
        :rtype: list
        """
        stateval_hori=[{} for t in range(hori+1)]
        poli_hori=[{} for t in range(hori+1)]
        for s in self.states.values():
            val_max = -math.inf
            arg_max = None
            for a in s.actions:
                tmpval = s.reward_of_a(a)
                if tmpval > val_max:
                    arg_max = a
                    val_max = tmpval
            stateval_hori[0][s.state_id] = val_max
            poli_hori[hori][s.state_id] = arg_max
        for t in range(1,hori+1):
            for s in self.states.values():
                val_max = -math.inf
                arg_max = None
                for a in s.actions:
                    tmpval = s.reward_of_a(a) + sum(
                        [stateval_hori[t-1][sp.state_id] *
                         s.trans_a_s(a, sp.state_id)
                         for sp in self.states.values()])
                    if tmpval>val_max:
                        arg_max = a
                        val_max = tmpval
                stateval_hori[t][s.state_id]=val_max
                poli_hori[hori-t][s.state_id]=arg_max
        return poli_hori


class State:
    """
    :param int id: Id of the state
    :param list actions: List of actions available
    :param dict reward: Reward for each possible action
    :param dict transition: Dictionary mapping a transition probability to each\
     combination of action and state
    """
    def __init__(self,id,actions,reward,transition):
        self.state_id=id
        self.actions=actions
        self.rewards=reward
        self.transition=transition

    def reward_of_a(self,a_id):
        """
        Return the reward for a given action

        :param int a_id: action id
        :return: Value of the reward
        :rtype: int
        """
        return self.rewards[a_id]

    def trans_a_s(self,a_id,s_id):
        """
        Return the transition for a given action and next state

        :param int a_id: action id
        :param int s_id: Next state id
        :return: Probability of transition
        :rtype: float
        """
        return self.transition[(a_id,s_id)]


# mdp=MDP([0,1,2],
#     [[0,1],[0,1],[0]],
#     [{0:-5,1:10},{0:5,1:0},{0:20}],
#     [{(0,0):0.9,(0,1):0,(0,2):0.1,(1,0):0,(1,1):1,(1,2):0},
#      {(0,0):0,(0,1):1,(0,2):0,(1,0):0.8,(1,1):0.2,(1,2):0},
#      {(0,0):0.9,(0,1):0.1,(0,2):0}]
#     )
#
# # print(mdp.get_actions_of_state(2))
# # print(mdp.get_trans_s_act_ns(2,0,2))
# # print(mdp.stateValueAlg(0.9,0.1))
# # print(mdp.backwardRecursion(5))
#
# mdpbis=MDP([0,1,2],
#     [[0,1],[0],[0]],
#     [{0:10,1:5},{0:10},{0:-100}],
#     [{(0,0):0,(0,1):1,(0,2):0,(1,0):1,(1,1):0,(1,2):0},
#      {(0,0):0.1,(0,1):0,(0,2):0.9},
#      {(0,0):0,(0,1):0,(0,2):1}]
#     )
# print(mdpbis.backwardRecursion(2))

#Change transition into ditcionary to avoid to much key
class SG:
    """
    Stochastic game class, attributes and methods

    :param list games: List of normal form game
    :param transition:
    states : dictionnary

    """

    def __init__(self, games, transition):
        # self.states: Associate to each stateID a gameState (StateSG),
        # a state with a game
        self.states = {}
        for g_i, g in enumerate(games):
            self.states[g_i] = StateSG(g_i, g, transition[g_i])

    # Backward Recursion
    # No discount
    def backwardRecursion(self, hori):
        """
        For a given horizom, solve a SG using backward dynamic programming

        :param hori: int
            Horizon for the current execution
        :return: Policy for each step
        :rtype: list of dict
        """
        # Create dictionary for both policy and value for all step
        stateval_hori = [{} for t in range(hori+1)]
        poli_hori = [{} for t in range(hori+1)]
        # Init state value for step T (horizon)
        for s_id in self.states.keys():
            sg = self.states[s_id]
            # Best action is a nash equilibria
            nash_equi = sg.solve_state()
            nash_equi_ord = [nash_equi[n] for n in range(sg.s_game.n_players)]
            # The value of the state is the expected util of the equilibrium
            equi_val = sg.s_game.expected_utilities(nash_equi)
            poli_hori[hori][s_id] = nash_equi_ord
            stateval_hori[hori][s_id] = equi_val
        for t in range(1, hori+1):
            for s_id in self.states.keys():
                sg = self.states[s_id]
                # The utility for a step t also depend on the value of the state
                # for t+1. A new game is created for the step t.
                tmpgame = self.modgame(sg,
                                       stateval_hori[hori+1-t],
                                       self.states.keys())
                # tmp_pmg = PMG(tmpgame.players_actions,
                #               [tmpgame.utilities], [[0, 1]])
                # lhpoly=LHpolymatrix(tmp_pmg)
                # nash=lhpoly.launch_solver([0,0])
                nash = self.solve_game(tmpgame)
                nash_equi_ord = [nash[n] for n in
                                 range(tmpgame.n_players)]
                equi_val = tmpgame.expected_utilities(nash)
                poli_hori[hori-t][s_id] = nash_equi_ord
                stateval_hori[hori-t][s_id] = equi_val
        print(stateval_hori)
        return poli_hori



    #For now only " players polymatrix games are solved, most effcient to test
    #Turn the bimatrix game into a polymatrix game,
    # LH for bimat not yet implented
    def solve_game(self, tmpgame):
        """
        For a given game compute a Nash equilibrium
        Currently works only for bimatrix game (use their polymatrix form)

        :param NFG tmpgame: Game to solve
        :return: Nash equilibrium (as a list of sublist)
        :rtype: list
        """
        tmp_pmg = PMG(tmpgame.players_actions,
                      [tmpgame.utilities], [[0, 1]])
        lhpoly = LHpolymatrix(tmp_pmg)
        return lhpoly.launch_solver([0, 0])

    # For the game/state at T-t and the value at T-t+1, compute the
    # modified game
    def modgame(self, sg, state_val, all_sgid):
        """
        Given the values for the state at a step t+1, give the updated reward
        for the game at a step t.

        :param dictionnary sg: State of the SG to compute for the current step
        :param StateSG state_val: Value of the state at the next step
        :param list all_sgid: id of all the states
        :return: A normal form game
        :rtype: NFG
        """
        utilcop = deepcopy(sg.s_game.utilities)
        jact = sg.s_game.joint_actions.tolist()
        for jact_i, jact in enumerate(jact):
            currentkey = ''.join(str(a) for a in jact)
            for n in range(sg.s_game.n_players):
                utilcop[n][jact_i] += sum([state_val[sp_id][n] *
                                           sg.transition[(currentkey, sp_id)]
                                           for sp_id in all_sgid])
        return NFG(sg.s_game.players_actions, utilcop)

    # Value Iteration Algorithm, pertinent? correct?
    # Check Bounded value iteration for SG, lower and upper bound
    def stateValueAlg(self, disco, epsi):
        """

        :param disco:
        :param epsi:
        :return:
        """
        stateval_dic={}
        for s_id in self.states.keys():
            state= self.states[s_id]
            nash_equi = state.solve_state()
            nash_equi_ord = [nash_equi[n]
                             for n in range(state.s_game.n_players)]
            equi_val = state.s_game.expected_utilities(nash_equi)
            stateval_dic[s_id] = equi_val
            #max([s.reward_of_a(a) for a in s.actions])
        policy = {}
        t = 0
        delta = math.inf
        while epsi < delta:
            t += 1
            delta = 0
            tmp_stateval={}
            for s_id in self.states.keys():
                state = self.states[s_id]
                # The utility for a step t also depend on the value of the state
                # for t+1. A new game is created for the step t.
                tmpgame = self.modgamedisco(state,
                                          stateval_dic,
                                          self.states.keys(),
                                          disco)
                nash = self.solve_game(tmpgame)
                nash_equi_ord = [nash[n] for n in
                                 range(tmpgame.n_players)]
                equi_val = tmpgame.expected_utilities(nash)
                tmp_stateval[s_id] = equi_val
                policy[s_id] = nash
                tmp_dif=max([abs(tmp_stateval[s_id][n]-stateval_dic[s_id][n])
                             for n in range(tmpgame.n_players)])
                delta = max(delta, tmp_dif)
            stateval_dic = tmp_stateval
        print(t)
        return policy

    def modgamedisco(self, state, state_val, all_sgid,disco):
        """

        :param state:
        :param state_val:
        :param all_sgid:
        :param disco:
        :return:
        """
        utilcop = deepcopy(state.s_game.utilities)
        jact = state.s_game.joint_actions.tolist()
        for jact_i, jact in enumerate(jact):
            currentkey = ''.join(str(a) for a in jact)
            for n in range(state.s_game.n_players):
                utilcop[n][jact_i] += \
                    disco*sum([state_val[sp_id][n] *
                               state.transition[(currentkey, sp_id)]
                               for sp_id in all_sgid])
        return NFG(state.s_game.players_actions, utilcop)


# State of a stochastic game, extension to State is useless currently
class StateSG:
    """
    Class use to implement a State in a stochastic game

    :param int s_id: State id
    :param NFG game: Normal form game of the state
    :param dict transition: Dictionary mapping to each joint action and \
    state transition probability
    """

    def __init__(self, s_id, game, transition):
        self.s_game = game
        self.state_id = s_id
        self.players_id = [n for n in range(self.s_game.n_players)]
        joint_act = game.joint_actions  # .tolist()
        self.rewards = {}  # {"000":# }
        self.transition = {}
        for jact_i, jact in enumerate(joint_act.tolist()):
            currentkey = ''.join(str(a) for a in jact)
            current_values = [self.s_game.util_of_joint_action(n, jact)
                              for n in self.players_id]
            current_transition = transition[jact_i]
            for ns in range(len(current_transition)):
                self.transition[(currentkey, ns)] = current_transition[ns]
            self.rewards[currentkey] = current_values

    def get_player_action(self, play_id):
        """
        For a player in the current state, return his set of actions

        :param int play_id: id/number of the player
        :return: Set of actions
        :rtype: list
        """
        return self.s_game.players_actions[play_id]

    # For now only " players polymatrix games are solved, most effcient to test
    # Turn the bimatrix game into a polymatrix game,
    # LH for bimat not yet implemented
    def solve_state(self):
        """
        For the game of the current state compute a Nash equilibrium
        Currently works only for bimatrix game (use their polymatrix form)

        :return:
        """
        tmp_pmg = PMG(self.s_game.players_actions,
                      [self.s_game.utilities], [[0, 1]])
        lhpoly = LHpolymatrix(tmp_pmg)
        return lhpoly.launch_solver([0, 0])


# nfgbis = NFG([[0, 1], [0, 1]], [[2, 0, 3, 4], [0, 1, 5, 4]])
# nfgbis2 = NFG([[0, 1], [0, 1]], [[3, 5, 1, 4], [4, 1, 1, 2]])
# nfgbis3 = NFG([[0, 1], [0, 1]], [[3, 2, 6, 7], [6, 5, 0, 3]])
#
# probatrans = [[[1, 0, 0],[0,0.5,0.5],[0.25,0.25,0.5],[0,0,1]],
#             [[0, 1, 0],[0.5,0,0.5],[0.5,0.25,0.25],[1,0,0]],
#             [[0, 0, 1],[0.5,0.5,0],[0.25,0.5,0.25],[0,1,0]]]
#
# # sg1=StateSG(0,nfgbis,[[1, 0, 0],[0,0.5,0.5],[0.25,0.25,0.5],[0,0,1]])
#
# sgtest = SG([nfgbis, nfgbis2, nfgbis3], probatrans)
#
# sgtest.backwardRecursion(6)


