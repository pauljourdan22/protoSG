import setuptools
setuptools.setup(
    name='gtsg',
    version='0.1',
    description='Implementation of stochastic game and monitoring game',
    author='Paul Jourdan',
    packages=setuptools.find_packages()
)